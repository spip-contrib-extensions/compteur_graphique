<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'compteurgraphique_slogan' => 'Affiche des compteurs graphiques de visites',
'compteurgraphique_description' => "Ce plugin, construit dans le cadre du d&#233;veloppement du squelette EVA-web,
se propose de g&#233;n&#233;rer des images de compteurs de visites param&#233;trables aussi bien au niveau graphique que fonctionnel.
Il peut s'int&#233;grer &#224; n'importe quel site fonctionnant sous SPIP.
{Certains habillages sont issus du site [Icons Etc->http://icons.mysitemyway.com/]}",
);
?>